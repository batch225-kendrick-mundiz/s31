//nodejs introductio

//use the require directive load node.js modules
// the http module lets node js transfer data using the http
let http = require("http");

http.createServer(function (req, response) {

    if(req.url == "/greeting"){
        response.writeHead(200, {'Content-type' : 'text/plain'});

        response.end("Welcome to my page");
    }else if(req.url == "/customer-service"){
        response.writeHead(200, {'Content-type' : 'text/plain'});

        response.end("Welcome to customer service");
    }else{
        response.writeHead(404, {'Content-type' : 'text/plain'});

        response.end("Page not available");
    }
    
}).listen(4000)

console.log("Server is running at localhost 4000");