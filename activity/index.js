// 1. What directive is used by Node.js in loading the modules it needs?
//require


// 2. What Node.js module contains a method for server creation?
//http module


// 3. What is the method of the http object responsible for creating a server using Node.js?
//createServer method


// 4. What method of the response object allows us to set status codes and content types?
//writeHead method


// 5. Where will console.log() output its contents when run in Node.js?
//cmd


// 6. What property of the request object contains the address's endpoint?
//url property
let http = require("http");

http.createServer(function (req, response) {

    if(req.url == "/login"){
        response.writeHead(200, {'Content-type' : 'text/plain'});

        response.end("Welcome to the login page");
    }else{
        response.writeHead(404, {'Content-type' : 'text/plain'});

        response.end("Page not found");
    }
    
}).listen(4000)

console.log("Server is running at localhost 4000");